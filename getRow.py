import csv

def main(yields, mode, sample):
  
  pm = u"\u00B1"
  nA = str(round(yields[0], 1))
  nB = str(round(yields[1], 1))
  nC = str(round(yields[2], 1))
  nD = str(round(yields[3], 1))
  if 'Data' not in sample:
    nA = nA + "±" + str(round(yields[5], 1))
    nB = nB + "±" + str(round(yields[6], 1))
    nC = nC + "±" + str(round(yields[7], 1))
    nD = nD + "±" + str(round(yields[8], 1))
    nA_est = ""
  else: nA_est = str(round(yields[4], 1)) + "±" + str(round(yields[9], 1))
  row = [sample, nA, nB, nC, nD, nA_est]
  return row 


  
