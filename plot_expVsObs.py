import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import csv
import run # ABCD framework runfile
import mplhep as hep
import argparse
from scipy.interpolate import make_interp_spline


def get_nAs(nAs):
  with open(outDir+"output_T.csv", "r") as inFile:
    reader = csv.reader(inFile, delimiter=",")
    count = 0
    for row in reader:
      if count == 1: nAs[0].append(row[-1]) # append observed
      if count == 5: nAs[1].append(row[-1]) # append expected
      count+=1
  inFile.close()
  return nAs

def isoID(region, mode):
  nAs = [[],[]]
  print("\n")
  print("Running over leading muDPJ isoID values...")
  if region == "BC" or region == "VR" or region == "nominal": cuts = [0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5] 
  if region == "DC": cuts = [2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5, 7]
  for cut in cuts:
    print("Current leading muDPJ cut at " + str(cut))
    print("\n")
    if region == "BC": run.main(mode, [cut, 2])
    if region == "DC" or region == "VR": run.main(mode, [cut, 1])
    nAs = get_nAs(nAs)
  print("nA_obs: " + str(nAs[0]))
  print("nA_exp: " + str(nAs[1]))
  print("\n")
  plot(region, cuts, 1, nAs, sliding_var="#Sigma_{#DeltaR=0.5} p_{T} [GeV]")

def plot(region, cuts, static_cut, nAs, sliding_var):
 
  hep.style.use(hep.style.ATLAS)
  print("Now plotting...")
  print("\n")
  obs_nAs = [float(nA) for nA in nAs[0]] # no error on observed nA!
  obs_errs = [np.sqrt(obs_nA) for obs_nA in obs_nAs]
  obs_errsUp = [(obs_nAs[i] - obs_errs[i]) for i in range(len(obs_nAs))]
  obs_errsDown = [(obs_nAs[i] + obs_errs[i]) for i in range(len(obs_nAs))]
  exp_nAs = [float(exp_nA.split("±")[0]) for exp_nA in nAs[1]]
  exp_errs = [float(exp_nA.split("±")[1]) for exp_nA in nAs[1]]
  exp_errsUp = [(exp_nAs[i] - exp_errs[i]) for i in range(len(exp_nAs))]
  exp_errsDown = [(exp_nAs[i] + exp_errs[i]) for i in range(len(exp_nAs))]
  fig, ax = plt.subplots()
  if 'sub-region' in mode and 0.5 in cuts:
    SR = 'BC1'
    x = 0.7
    # for Nominal||MET trigger inclusive
    plt.text(x,51,'$\\mathrm{\\geq 1\\mu}$DPJ VBF channel')
    plt.text(x,48,'BC subplane, $\\mathrm{E_T^{miss}}$>20GeV')

  elif 'sub-region' in mode: 
    SR = 'DC1'
    x = 2.7
    plt.text(x,102,'$\\mathrm{\\geq 1\\mu}$DPJ VBF channel')
    plt.text(x,96,'DC subplane, $E_T^{miss}$>20GeV')
  
  else:
    SR = 'A\''
    x = 0.7
    plt.text(x,122,'$\\mathrm{\\geq 1\\mu}$DPJ VBF channel')
    plt.text(x,116,'Inverted |$\\mathrm{\\Delta\\phi_{jj}}$|, no $\\mathrm{\\mu}$DPJ centrality VR')
    
  # interpolate to draw curved line between points
  # do this also for error bars 
  cuts_smooth = np.linspace(min(cuts), max(cuts), 45)
  obs_spl = make_interp_spline(cuts, obs_nAs)
  obsUp_spl = make_interp_spline(cuts, obs_errsUp)
  obsDown_spl = make_interp_spline(cuts, obs_errsDown)
  exp_spl = make_interp_spline(cuts, exp_nAs)
  expUp_spl = make_interp_spline(cuts, exp_errsUp)
  expDown_spl = make_interp_spline(cuts, exp_errsDown)

  obs_smooth  = obs_spl(cuts_smooth)
  obsUp_smooth = obsUp_spl(cuts_smooth)
  obsDown_smooth = obsDown_spl(cuts_smooth)
  exp_smooth  = exp_spl(cuts_smooth)
  expUp_smooth = expUp_spl(cuts_smooth)
  expDown_smooth = expDown_spl(cuts_smooth)

  # plot with markers at cut values (not all points used for interpolation
  plt.plot(cuts_smooth, obs_smooth, linestyle="-", linewidth="0.5", label="Observed " + SR, marker=".", markevery=5) # plot observed
  plt.plot(cuts_smooth, exp_smooth, linestyle="-", linewidth="0.5", label="Expected " + SR, marker=".", markevery=5) # plot expected

  # plot error bands and draw axis labels 
  hep.atlas.text(" Work In Progress")
  plt.fill_between(cuts_smooth, expUp_smooth, expDown_smooth, color="orange", alpha=0.2) 
  plt.fill_between(cuts_smooth, obsUp_smooth, obsDown_smooth, color="blue", alpha=0.2) 
  plt.xlabel("Cut on " + sliding_var, fontsize=20)
  plt.ylabel("Events", fontsize=20)

  plt.legend(loc=4)
  loc = "obsVsExp_"+region+".pdf"
  plt.savefig(outDir+loc)
  print("-------------------------------------------------------")
  print("Finished - plot saved to control-region_validation/"+loc)
  print("-------------------------------------------------------")
  #plt.clf()

parser = argparse.ArgumentParser()
parser.add_argument("--region", help="nominal, BC, DC, VR", type=str)
args = parser.parse_args()

if args.region == "nominal": mode = "estimation"
if args.region == "VR": mode = "control-region_validation"
if args.region == "DC" or args.region == "BC": mode = "sub-region_validation"

outDir = "/Users/s1891044/Documents/Physics/DarkPhotons/vbf-mudpj-abcd-framework/output/charge_isoID/"+mode+"/"
isoID(args.region, mode)

'''def charge(mode):
  xcut = 2
  nAs = [[],[]]
  print("\n")
  print("Running over muDPJ |charge| values...")
  cuts = [1, 2, 3]
  for ycut in cuts:
    print("Current muDPJ |charge| cut at " + str(ycut))
    print("\n")
    run.main(mode, [xcut, ycut], 100)
    nAs = get_nAs(nAs)
    print("nAs:" + str(nAs))
  print("nA_obs: " + str(nAs[0]))
  print("nA_exp: " + str(nAs[1]))
  print("\n")
  plot(cuts, xcut, nAs, sliding_var="muDPJ_charge")'''
