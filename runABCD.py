# Main program that runs ABCD framework
# Recieves input file, applies region specific cuts
# Calculates expected nA, signal leakage & errors
# Execute ABCD estimation of nA, deals with errors and signal leakage

from ROOT import TFile, TTree, TCanvas, TH2D, TLine, kRed
import math
import numpy as np 
import charge_isoID 

def main(inFile, mode, sample, cuts):

  # get TTree
  tfile = TFile(inFile)
  ttree = tfile.Get("miniT") 

  abcdYields = [] 
  pm = u"\u00B1"

  # call program extractYields.main() to return nevents in each region of interest 
  nA, nB, nC, nD, n, errA, errB, errC, errD, errN = charge_isoID.main(ttree, sample, mode, cuts)
  bgdYields = [nB, nC, nD]
  '''print("--------------------------------------------------------------------")
  if mode == "estimation": print("Extracting ABCD yields from " + sample + "...")
  if mode == "correlation": print("Extracting correlation from " + sample + "...")
  if "validation" in mode: print("Validating ABCD framework on " + sample + "...")
  print("--------------------------------------------------------------------")'''
  nA_exp, errA_exp = 0, 0
  # SIGNAL: calculate signal leakage into CRs
  if "vbf" in sample:
    signalLeakage = [round((ni/n), 2) for ni in bgdYields]
    #print("Signal in nA: ", round(nA, 1), pm, (errA, 1))
    signalFraction = []
    for ni in [nA, nB, nC, nD]:
      if ni == 0: signalFraction.append(0)
      else: signalFraction.append(round(ni/n, 2))
    #print("Signal fraction into [A, B, C, D]: ", (signalFraction, 2))
    #print("Signal leakage into CRs [B, C, D] = " + str(signalLeakage))
    nA_exp, errA_exp = 0, 0
  # DATA: perform ABCD calculation 
  if "Data" in sample:
    print("nA, nB, nC, nD: ", nA, nB, nC, nD)
    nA_exp = (nB/nC)*nD
    # error propagation - systematic only (from systematic on B, C, D)
    if any([n == 0 for n in bgdYields]): errA_exp = 0 
    else: errA_exp = nA_exp*math.sqrt((1/nB) + (1/nC) + (1/nD))
    print("Background estimation in nA: ", round(nA_exp, 2), pm , round(errA_exp, 2))
    if mode != 'estimation': print("Observed nA: ", round(nA,2), pm , round(errA,2))
  abcdYields = [nA, nB, nC, nD, nA_exp, errA, errB, errC, errD,  errA_exp]
  return abcdYields 
