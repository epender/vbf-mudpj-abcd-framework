# Script to transform output csv file to latex table format

import csv

def main():

  outdir = "/Users/s1891044/Documents/Physics/DarkPhotons/vbf-mudpj-abcd-framework/output/charge_isoID/control-region_validation/"
  #outdir = "/Users/s1891044/Documents/Physics/DarkPhotons/vbf-mudpj-abcd-framework/output/charge_isoID/sub-region_validation/BC/"
  #outdir = "/Users/s1891044/Documents/Physics/DarkPhotons/vbf-mudpj-abcd-framework/output/charge_isoID/sub-region_validation/DC/"
  #outdir = "/Users/s1891044/Documents/Physics/DarkPhotons/vbf-mudpj-abcd-framework/output/charge_isoID/estimation/"
  print("Converting ABCD yields csv output file to latex format... ")
  with open(outdir+"output_T.csv", "r") as infile, open(outdir+"output.txt", "w") as outfile:
    rows = []
    row_count = 0
    reader = csv.reader(infile)
    line2 = ""
    for row in reader:
      line = ""
      if row_count == 0: row[0] = '\\multirow{2}{*}{ABCD Yield}'
      for entry in range(len(row)-1): line += row[entry] + " & " 
      line += row[-1] + " \\\\ \n"
      line = line.replace("±", "$\pm$")
      line = line.replace("vbf 500757", "$\\mathrm{m(\\gamma_{d})=0.1GeV}$")
      line = line.replace("vbf 500758", "$\\mathrm{m(\\gamma_{d})=0.4GeV}$")
      line = line.replace("vbf 500759", "$\\mathrm{m(\\gamma_{d})=0.4GeV}$")
      line = line.replace("vbf 500760", "$\\mathrm{m(\\gamma_{d})=0.4GeV}$")
      line = line.replace("vbf 500761", "$\\mathrm{m(\\gamma_{d})=10GeV}$")
      line = line.replace("vbf 500762", "$\\mathrm{m(\\gamma_{d})=15GeV}$")
      line = line.replace("Run 2 Data", "\multirow{2}{*}{Run 2 Data}")
      line = line.replace("vbf 521257", "$\\mathrm{m(\\gamma_{d})=0.017GeV}$")
      line = line.replace("vbf 521258", "$\\mathrm{m(\\gamma_{d})=0.05GeV}$")
      line = line.replace("vbf 521259", "$\\mathrm{m(\\gamma_{d})=0.9GeV}$")
      line = line.replace("vbf 521260", "$\\mathrm{m(\\gamma_{d})=2GeV}$")
      line = line.replace("vbf 521261", "$\\mathrm{m(\\gamma_{d})=6GeV}$")
      line = line.replace("vbf 521262", "$\\mathrm{m(\\gamma_{d})=25GeV}$")
      line = line.replace("vbf 521263", "$\\mathrm{m(\\gamma_{d})=40GeV}$")
      rows.append(line)
      if row_count == 0: 
         nFiles = len(row)-1
         if "0.017GeV" in line:
           line2 += ' & $\\mathrm{c\\tau=2mm}$'
           line2 += ' & $\\mathrm{c\\tau=7mm}$'
           line2 += ' & $\\mathrm{c\\tau=15mm}$'
           line2 += ' & $\\mathrm{c\\tau=50mm}$'
           line2 += ' & $\\mathrm{c\\tau=115mm}$'
           line2 += ' & $\\mathrm{c\\tau=175mm}$'
           line2 += ' \\\\ \\hline \n' 
           #line2 += ' & $\\mathrm{c\\tau=50mm}$'
           #line2 += ' & $\\mathrm{c\\tau=5mm}$'
           #line2 += ' & $\\mathrm{c\\tau=500mm}$'
           #line2 += ' & $\\mathrm{c\\tau=900mm}$'
           #line2 += ' & $\\mathrm{c\\tau=1000mm}$'
           #line2 += ' & \\\\ \\hline \n' 
         else:
           line2 += ' & $\\mathrm{c\\tau=600mm}$'
           line2 += ' & $\\mathrm{c\\tau=900mm}$'
           line2 += ' & $\\mathrm{c\\tau=1000mm}$'
           line2 += ' & $\\mathrm{c\\tau=1200mm}$'
           line2 += ' & $\\mathrm{c\\tau=1400mm}$'
           line2 += ' & \\\\ \\hline \n' 
           #line2 += ' & $\\mathrm{c\\tau=2mm}$'
           #line2 += ' & $\\mathrm{c\\tau=7mm}$'
           #line2 += ' & $\\mathrm{c\\tau=115mm}$'
           #line2 += ' & $\\mathrm{c\\tau=175mm}$'
           #line2 += ' & $\\mathrm{c\\tau=600mm}$'
           #line2 += ' & $\\mathrm{c\\tau=1200mm}$'
           #line2 += ' & $\\mathrm{c\\tau=1400mm}$'
           #line2 += ' \\\\ \\hline \n' 
         rows.append(line2)
      row_count += 1
    outfile.write('\\begin{table}\n')
    outfile.write('\\resizebox{\\linewidth}{!}{\n')
    outfile.write('\\centering\n')
    outfile.write('\\begin{tabular}{|c|')
    for n in range(nFiles): outfile.write('c|')
    outfile.write('}\n\\hline \n')
    for row in rows: outfile.write(row)
    outfile.write('\\hline \n')
    outfile.write('\\end{tabular}} \n')
    outfile.write('\\caption{ABCD yields in VBF signal and data driven estimate for background} \n')
    outfile.write('\\end{table}')
    print("output.txt created at "+outdir+"output.txt")

main()
