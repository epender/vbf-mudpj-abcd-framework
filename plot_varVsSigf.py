# Script will run over ABCD framework for specified number of MET cuts
# Extracting signal and error on expected background to calculate a preliminary significance
# Plot significance vs. MET distribution for desired signal samples

import matplotlib.pyplot as plt
import numpy as np
import csv

import run

outDir = "/Users/s1891044/Documents/Physics/DarkPhotons/vbf-mudpj-abcd-framework/output/charge_isoID/estimation/"

def met():
  met_cuts = np.arange(80, 240, 10)
  signal, errB, sigfs = [], [], []
  for cut in met_cuts:
    print("Current MET cut: " + str(cut) + "GeV")
    run.main("estimation", [4.5, 1], cut)
    with open(outDir+"output_T.csv", "r") as inFile:
      reader = csv.reader(inFile, delimiter=",")
      count = 0
      for row in reader:
        if count == 1: signal.append(row[1].split("±")[0]) # append observed
        if count == 5: errB.append(row[-1].split("±")[1]) # append expected
        count+=1
    inFile.close()
  print("signal[0]: "+str(signal[0]))
  print("errB[0]: "+str(errB[0]))
  for idx in range(len(signal)): sigfs.append(float(signal[idx])/float(errB[idx]))
  plot(met_cuts, "met", sigfs)

def dphijj():
  dphijj_cuts = np.arange(0.1, 3.2, 0.1)
  signal, errB, sigfs = [], [], []
  for cut in dphijj_cuts:
    print("Current |dphijj| cut: " + str(cut))
    run.main("estimation", [4.5, 1], cut)
    with open(outDir+"output_T.csv", "r") as inFile:
      reader = csv.reader(inFile, delimiter=",")
      count = 0
      for row in reader:
        if count == 1: signal.append(row[1].split("±")[0]) # append observed
        if count == 5: errB.append(row[-1].split("±")[1]) # append expected
        count+=1
    inFile.close()
  print("signal[0]: "+str(signal[0]))
  print("errB[0]: "+str(errB[0]))
  for idx in range(len(signal)): sigfs.append(float(signal[idx])/float(errB[idx]))
  plot(dphijj_cuts, "dphijj", sigfs)

def isoID():
  isoID_cuts = np.arange(0.1, 5, 0.1)
  signal, errB, sigfs = [], [], []
  for cut in isoID_cuts:
    print("Current isoID cut: " + str(cut))
    run.main("estimation", [cut, 1], 100)
    with open(outDir+"output_T.csv", "r") as inFile:
      reader = csv.reader(inFile, delimiter=",")
      count = 0
      for row in reader:
        if count == 1: signal.append(row[1].split("±")[0]) # append observed
        if count == 5: errB.append(row[-1].split("±")[1]) # append expected
        count+=1
    inFile.close()
  print("signal[0]: "+str(signal[0]))
  print("errB[0]: "+str(errB[0]))
  
  for idx in range(len(signal)): sigfs.append(float(signal[idx])/float(errB[idx]))
  plot(isoID_cuts, "isoID", sigfs)

def plot(cuts, var, sigfs):
  
  ax = plt.plot(cuts, sigfs, label="500758")
  plt.xlabel(var)
  plt.ylabel("Significance (n(S)/dn(B)")
  plt.title(var+" vs. Significance (VBF 400MeV Sample/Error on ABCD Prediction)")
  plt.savefig(outDir+var+"-vs-sigf.png")
  #plt.show()
  print("------------------------------------------------------")
  print("Finished - plot saved to "+var+"-vs-sigf.png")
  print("------------------------------------------------------")
  plt.clf()

isoID()
#met()
#dphijj()
