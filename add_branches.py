# This script runs after the VBF tagger dresser
# Adds LJmu1 quality cuts with inverted LJmuon_CNNscore   
# Branch required to form QCD enriched region with negligible signal
# For VBF muDPJ ABCD validation

from ROOT import TFile, TTree, TH1D, TLorentzVector, TVector2
import sys, os, math
import numpy as np
import multiprocessing as mp
from array import array
from operator import itemgetter
import csv

def setup():

    inputDir = "/Users/s1891044/Documents/Physics/DarkPhotons/source/v02-00/"
    if not os.path.isdir(inputDir):
        print(inputDir + " does not exist, exitting")
        exit()
    # specifiy output
    outputDir = "/Users/s1891044/Documents/Physics/DarkPhotons/source/v02-00/abcd-samples/"
    if not os.path.isdir(outputDir):
        os.makedirs(outputDir)
    print("output directory set to be " + outputDir)
    tree = "miniT" 

    # access each root file in the inputDir
    # apply cuts on the tree and save the output in the outputDir
    files = []
    outputfiles = []
    for file in os.listdir(inputDir):
        #if 'jets' in file:
        #if 'vbf' in file:
        #if 'qcd' in file or 'data' in file or 'diboson' in file or 'top' in file:
            files.append(inputDir + file)
            outputfiles.append(outputDir + file)
    pool = mp.Pool(len(files))
    results = [pool.apply_async(vbfdress, (files[i], outputfiles[i], tree, i)) for i in range(len(files))]
    pool.close()
    pool.join()


def vbfdress(file, outputname, tree, index):

    metTrig_weights = []
    with open("signal_scaleFactors.csv", "r") as sf_file:
      reader = csv.reader(sf_file)
      for row in reader:
        metTrig_weights.append(row[1])

    met_list = [100e3, 110e3, 120e3, 130e3, 140e3, 150e3, 165e3, 180e3, 200e3, 255e3]

    print("Processing", file, tree, outputname)
    tfile = TFile(file)
    ttree = tfile.Get(tree)
    if not ttree:
        print("no valid input tree, exitting")
        return 
    histos = [key.GetName() for key in tfile.GetListOfKeys() if key.GetClassName()[:2]=="TH"]
    tfile_out = TFile(outputname, "recreate")
    for x in histos:
        tfile_out.WriteTObject(tfile.Get(x))
    ttree_out = ttree.CloneTree(0)
    ttree_out.SetDirectory(tfile_out)
    
    m_LJmu1_isCR = array("i",[0])
    m_LJmu1_mu1DNNscore = array("d",[0])
    m_LJmu1_centrality = array("d",[0])
    m_LJmu1_charge = array("i",[0])
    m_LJjet1_centrality = array("d",[0])
    m_LJjet1_charge = array("i",[0])
    m_met_weight = array("d",[0])
    m_isZ_event = array("i",[0])
    m_Zmu1_idx = array("i",[0])
    m_Zmu2_idx = array("i",[0])
    m_dimuon_met = array("d",[0])
    m_LJ1_centrality = array("d",[0])
    m_abs_dphijj = array("d",[0])
    m_LJ1_type = array("i",[0])

    LJmu1_isCR_branch = ttree_out.Branch( 'LJmu1_isCR', m_LJmu1_isCR, 'LJmu1_isCR/I' )
    LJmu1_mu1DNNscore_branch = ttree_out.Branch( 'LJmu1_mu1DNNscore', m_LJmu1_mu1DNNscore, 'LJmu1_mu1DNNscore/D' )
    LJmu1_centrality_branch = ttree_out.Branch( 'LJmu1_centrality', m_LJmu1_centrality, 'LJmu1_centrality/D' )
    LJmu1_charge_branch = ttree_out.Branch( 'LJmu1_charge', m_LJmu1_charge, 'LJmu1_charge/I' )
    LJjet1_centrality_branch = ttree_out.Branch( 'LJjet1_centrality', m_LJjet1_centrality, 'LJjet1_centrality/D' )
    LJjet1_charge_branch = ttree_out.Branch( 'LJjet1_charge', m_LJjet1_charge, 'LJjet1_charge/I' )
    met_weight_branch = ttree_out.Branch( 'met_weight', m_met_weight, 'met_weight/D' )
    isZ_event_branch = ttree_out.Branch( 'isZ_event', m_isZ_event, 'isZ_event/I' )
    Zmu1_idx_branch = ttree_out.Branch( 'Zmu1_idx', m_Zmu1_idx, 'Zmu1_idx/I' )
    Zmu2_idx_branch = ttree_out.Branch( 'Zmu2_idx', m_Zmu2_idx, 'Zmu2_idx/I' )
    dimuon_met_branch = ttree_out.Branch( 'dimuon_met', m_dimuon_met, 'dimuon_met/D' )
    LJ1_centrality_branch = ttree_out.Branch( 'LJ1_centrality', m_LJ1_centrality, 'LJ1_centrality/D' )
    abs_dphijj_branch = ttree_out.Branch( 'abs_dphijj', m_abs_dphijj, 'm_abs_dphijj/D' )
    LJ1_type_branch = ttree_out.Branch( 'LJ1_type', m_LJ1_type, 'm_LJ1_type/I' )
    
    sumOfWeights_nEvents = tfile.numEvents.GetBinContent(1)
    # this is the default sum of weight 
    sumOfWeights = tfile.numEvents.GetBinContent(2)
    #count=0
    for entry in range(ttree.GetEntries()):
      #count+=1
      #if count != 231: continue
      ttree.GetEntry(entry)
      if entry > 0 and entry%100000==0: print("Processed {} of {} entries".format(entry,ttree.GetEntries()))
      m_LJmu1_isCR[0] = 1
      m_LJmu1_mu1DNNscore[0] = -1
      m_LJmu1_centrality[0] = -1
      m_LJmu1_charge[0] = -99
      m_LJjet1_centrality[0] = -1
      m_LJjet1_charge[0] = -99
      m_met_weight[0] = 1 # default is 1 so it has no effect on other samples
      m_isZ_event[0] = 0
      m_Zmu1_idx[0] = -1
      m_Zmu2_idx[0] = -1
      m_dimuon_met[0] = -99
      m_LJ1_centrality[0] = -1
      m_abs_dphijj[0] = abs(ttree.dphijj)
      m_LJ1_type[0] = -99
      
      if "vbf" in file: 
        if ttree.MET < 100e3: m_met_weight[0] = 0 
        if ttree.MET > 100e3 and ttree.MET < 225e3 and ttree.metTrig==1 and ttree.muDPJTrig!=1: 
          for met_idx in range(len(metTrig_weights)):
            if ttree.MET > met_list[met_idx] and ttree.MET < met_list[met_idx+1]: m_met_weight[0] = float(metTrig_weights[met_idx])
      if abs(m_met_weight[0]) > 1: print(m_met_weight[0], ttree.MET)
      LJmus20 = []
      LJjets20 = []
      # for centrality calculation
      square_dj = (ttree.jet1_eta - ttree.jet2_eta)**2
      half_jet_sum = (ttree.jet1_eta + ttree.jet2_eta)/2
      for idx_LJ in range(ttree.ptLJ.size()): 
        if ttree.ptLJ.at(idx_LJ) > 20e3 and ttree.types.at(idx_LJ) == 0: LJmus20.append(idx_LJ)
        if ttree.ptLJ.at(idx_LJ) > 20e3 and ttree.types.at(idx_LJ) == 2: LJjets20.append(idx_LJ)
      ttree_out.GetEntry(entry)
      xs = ttree.amiXsection
      weight = ttree.weight
      # Apply additional LJmu1 quality cuts
      if len(LJmus20) > 0:
        # find leading muonic DPJ centrality (wrt. vbf jets) 
        LJmu1_centrality = np.exp(-(4/square_dj)*((ttree.LJmu1_eta - half_jet_sum)**2))
        m_LJmu1_centrality[0] = LJmu1_centrality
        # find leading LJmu index
        idx_LJmu1 = LJmus20[0]
        for idx_LJ in range(len(ttree.ptLJ)): 
          if ttree.ptLJ.at(idx_LJ) > ttree.ptLJ.at(idx_LJmu1): idx_LJmu1 = idx_LJ
        LJmuons_charge = []
        # access muons associated with the leading muon DPJ
        for mu_idx in range(ttree.LJmuon_index.size()):
          if ttree.LJmuon_index.at(mu_idx) == idx_LJmu1:
            LJmuons_charge.append(ttree.LJmuon_charge.at(mu_idx))
            LJmu_eta = abs(ttree.LJmuon_eta.at(mu_idx))
            mu1_DNNscore = ttree.LJmuon_DNNscore.at(mu_idx)
            m_LJmu1_mu1DNNscore[0] = mu1_DNNscore
            if LJmu_eta > 1.0 and LJmu_eta < 1.1: m_LJmu1_isCR[0] = 0
            # Does LJmu1 pass quality cuts? 
            if ttree.LJmuon_author.at(mu_idx) == 1 or (mu1_DNNscore < 0.2 or mu1_DNNscore > 0.5): m_LJmu1_isCR[0] = 0
        # sum of charge of leading muDPJ constituents
        m_LJmu1_charge[0] = abs(sum(LJmuons_charge))
      # Apply additional LJjet1 quality cuts
      if len(LJjets20) > 0:
        # find leading calo DPJ centrality (wrt. vbf jets) 
        LJjet1_centrality = np.exp(-(4/square_dj)*((ttree.LJjet1_eta - half_jet_sum)**2))
        m_LJjet1_centrality[0] = LJjet1_centrality
        if len(LJmus20) == 0: m_LJ1_centrality[0] = LJjet1_centrality
        if len(LJmus20) > 0: 
          if ttree.LJmu1_pt > ttree.LJjet1_pt: m_LJ1_centrality[0] = LJmu1_centrality
          else: m_LJ1_centrality[0] = LJjet1_centrality
      if len(LJmus20) > 0 and len(LJjets20) == 0: m_LJ1_centrality[0] = LJmu1_centrality
      
      # define channel by type of leading DPJ - find leading DPJ index
      if len(ttree.ptLJ) == 1: m_LJ1_type[0] = ttree.types.at(0)
      if len(ttree.ptLJ) > 1:
        ptLJ = []
        for pT in ttree.ptLJ: ptLJ.append(pT)  
        indices, ptLJ_sorted = zip(*sorted(enumerate(ptLJ), key=itemgetter(1)))
        list(ptLJ_sorted)
        list(indices)
        #print(ptLJ_sorted, indices)
        m_LJ1_type[0] = ttree.types.at(indices[-1]) 
      #print(m_LJ1_type[0])
      #print("") 
      # if Z-->mumu event, get new MET with dimuon pT
      #----------------------------------------------
      Zmu_info = Zmus_info(ttree, entry)
      m_dimuon_met[0] = Zmu_info[0]
      m_Zmu1_idx[0] = Zmu_info[1]
      m_Zmu2_idx[0] = Zmu_info[2]
      if Zmu_info[0] == -99: m_isZ_event[0] = 0
      else: m_isZ_event[0] = 1
      #-----------------------
      ttree_out.Fill()
    print("ttree filled")
    tfile.Close()
    
    print ("")
    print ("SAMPLE " + file.rsplit("/", 1)[1])
    print ("SumOfWeights: " + str(sumOfWeights))
    tfile_out.Write()
    tfile_out.Close()

if __name__=='__main__':
    setup()            

def Zmus_info(ttree, entry):
  mu_idxs = []
  Zmu1_idx = -1
  Zmu2_idx = -1
  isZ = False
  dimuon_met_pT = -99
  # single Z muon selection
  for mu_idx in range(len(ttree.muon_pt)):
    mu_pt = ttree.muon_pt.at(mu_idx) > 27e3                                           # minimum muon pT 
    mu_eta = abs(ttree.muon_eta.at(mu_idx)) < 2.5                                     # muon eta cut
    mu_iso = ttree.muon_author.at(mu_idx) == 1                                        # require isolated muons
    mu_comb = ttree.muon_type.at(mu_idx) == 0                                         # require combined muons
    if mu_pt and mu_eta and mu_iso and mu_comb: mu_idxs.append(mu_idx) 
  
  # dimuon candidate selection 
  # -----------------------------
  # if only 2 muons, straight forward to calculate dimuon quantities
  if len(mu_idxs) == 2:
    mu1_p4 = TLorentzVector(0, 0, 0, 0)
    mu2_p4 = TLorentzVector(0, 0, 0, 0)
    mu1_p4.SetPtEtaPhiE(ttree.muon_pt.at(mu_idxs[0]), ttree.muon_eta.at(mu_idxs[0]), ttree.muon_phi.at(mu_idxs[0]), ttree.muon_e.at(mu_idxs[0]))
    mu2_p4.SetPtEtaPhiE(ttree.muon_pt.at(mu_idxs[1]), ttree.muon_eta.at(mu_idxs[1]), ttree.muon_phi.at(mu_idxs[1]), ttree.muon_e.at(mu_idxs[1]))
    dimuon_mass = (mu1_p4 + mu2_p4).M()
    dimuon_charge = ttree.muon_charge.at(mu_idxs[0]) + ttree.muon_charge.at(mu_idxs[1])
    if dimuon_mass > 66e3 and dimuon_mass < 116e3 and dimuon_charge == 0:
      #dimuon_pT = (mu1_p4 + mu2_p4).Pt()
      #dimuon_dR = mu1_p4.DeltaR(mu2_p4)
      Zmu1_idx = mu_idxs[0]
      Zmu2_idx = mu_idxs[1]
      isZ = True
  # if more than two muons, calculate dimuon quanitites for every combination
  elif len(mu_idxs) > 2: 
    for mu1 in mu_idxs:
      mu1_p4 = TLorentzVector(0, 0, 0, 0)
      mu1_p4.SetPtEtaPhiE(ttree.muon_pt.at(mu1), ttree.muon_eta.at(mu1), ttree.muon_phi.at(mu1), ttree.muon_e.at(mu1))
      for mu2 in mu_idxs:
        if (mu2 == mu1) or (isZ == True): continue
        mu2_p4 = TLorentzVector(0, 0, 0, 0)
        mu2_p4.SetPtEtaPhiE(ttree.muon_pt.at(mu2), ttree.muon_eta.at(mu2), ttree.muon_phi.at(mu2), ttree.muon_e.at(mu2))
        dimuon_mass = (mu1_p4 + mu2_p4).M()
        dimuon_charge = ttree.muon_charge.at(mu1) + ttree.muon_charge.at(mu2)
        if dimuon_mass < 66e3 or dimuon_mass > 116e3 or dimuon_charge != 0: continue
        Zmu1_idx = mu1
        Zmu2_idx = mu2
        isZ = True
  
  if isZ:
    met_2vec = TVector2(0, 0)
    Zmu1_2vec = TVector2(0, 0)
    Zmu2_2vec = TVector2(0, 0)
    met_2vec.SetMagPhi(ttree.MET, ttree.MET_phi) 
    Zmu1_2vec.SetMagPhi(ttree.muon_pt.at(Zmu1_idx), ttree.muon_phi.at(Zmu1_idx)) 
    Zmu2_2vec.SetMagPhi(ttree.muon_pt.at(Zmu2_idx), ttree.muon_phi.at(Zmu2_idx)) 
    dimuon_met = met_2vec + Zmu1_2vec + Zmu2_2vec
    dimuon_met_pT = np.sqrt((dimuon_met.Px()**2) + (dimuon_met.Py()**2))
  
  return [dimuon_met_pT, Zmu1_idx, Zmu2_idx]
 
