# Program to count number of events in each ABCD region
# Plots ABCD plane for signal and data
# Returns yields

from ROOT import TH2D, TLine, TCanvas, TTree, TFile, kRed, TLatex, TPaveText, gPad, gStyle, gROOT
import ctypes
import math
import numpy as np
import atlasplots as aplt

aplt.set_atlas_style()

def main(ttree, infile, mode, cuts):

  # Set default vars for nA estimation
  # isoID
  xbins = 20
  xmin = 0 
  xcut = cuts[0]
  xmax = 20

  # |charge|
  ybins = 5 
  ymin = 0 #2.5
  ycut = cuts[1]
  ymax = 5

  if "data" not in infile.lower(): weights = "(scale1fb*intLumi*metTrig_weight)"
  else: weights = "(scale1fb*intLumi)"
  vbfFilter = "(njet30>1&&mjj>1e6&&abs(detajj)>3)"
  trigger = "(muDPJTrig||metTrig)"
  metVR = "(MET>20e3)" 
  minMET = "(MET>100e3)" 
  dpjContent = "(LJ1_type==0)"
  leptonVeto = "(neleSignal==0&&nmuSignal==0)"
  bjetVeto = "(hasBjet==0)" 
  dpjQuality = "(LJmu1_isGood==1)"
  min_dphi_jetmet = "(min_dphi_jetmet>0.4)"
  dphijj = "(abs(dphijj)<2.5)"
  centrality = "(LJmu1_centrality>0.7)"
  baseline_selection = weights+"*("+vbfFilter+"&&"+trigger+"&&"+dpjContent+"&&"+leptonVeto+"&&"+bjetVeto
  # validation
  isCR = "(LJmu1_isCR==1)"
  invert_dphijj = "(abs(dphijj)>2.5)"
  invert_centrality = "(LJmu1_centrality<0.7)"
  centralityCR = "(LJmu1_centrality>0.2&&LJmu1_centrality<0.7)"
  BC = "(abs(LJmu1_charge)>=1)"
  DC = "(abs(LJmu1_isoID)>="+str(xcut)+")"
  # met trigger calibration
  tight_dphijj = "(abs(dphijj)<1.8)"  	
  tight_detajj = "(abs(detajj)>5.0)"  	

  fullmuonic_presel = baseline_selection+"&&"+minMET+"&&"+dpjQuality+"&&"+centrality+"&&"+dphijj 
  BC_presel = baseline_selection+"&&"+dpjQuality+"&&"+metVR+"&&"+centrality+"&&"+dphijj+"&&"+BC
  DC_presel = baseline_selection+"&&"+dpjQuality+"&&"+metVR+"&&"+centrality+"&&"+dphijj+"&&"+DC
  invertDNN_presel = baseline_selection+"&&"+isCR+"&&"+centrality
  dphijjVR_presel = baseline_selection+"&&"+minMET+"&&"+dpjQuality+"&&"+invert_dphijj
  centralityVR_presel = baseline_selection+"&&"+dpjQuality+"&&"+invert_centrality
  VR_presel = baseline_selection+"&&"+minMET+"&&"+dpjQuality
  LJmuon_isCR_presel = baseline_selection+"&&"+dpjQuality+"&&"+centralityCR
  tight_presel = fullmuonic_presel+"&&"+tight_dphijj+"&&"+tight_detajj

  print("ymin, ycut", ymin, ycut)
  if "estimation" in mode: presel = fullmuonic_presel 
  if "correlation" in mode: presel, xmin, xbins = fullmuonic_presel, 4.5, 31 
  if "control-region" in mode: presel = dphijjVR_presel  
  if ycut == 1 and "sub-region" in mode: presel, xmin, xbins = DC_presel, 2, 18 
  if ycut == 2 and "sub-region" in mode: presel, ymin, ybins = BC_presel, 1, 4 
  
  print("presel: ", presel)
   
  # blinding cut
  #if "Data" in infile and mode == "estimation": presel += "&&(abs(LJmu1_charge)>="+str(ycut)+"||LJmu1_isoID*0.001>="+str(xcut)+")"
  
  # get nevents in each region
  hA = TH2D("hA", "hA", xbins, xmin, xcut, ybins, ymin, ycut)
  hA.Sumw2()
  hB = TH2D("hB", "hB", xbins, xmin, xcut, ybins, ycut, ymax)
  hB.Sumw2()
  hC = TH2D("hC", "hC", xbins, xcut, xmax, ybins, ycut, ymax)
  hC.Sumw2()
  hD = TH2D("hD", "hD", xbins, xcut, xmax, ybins, ymin, ycut)
  hD.Sumw2()
  h = TH2D("h_"+infile, "LJmu1_isoID*0.001:abs(LJmu1_charge) ", xbins, xmin, xmax, ybins, ymin, ymax)
  h.Sumw2()
  #print("ymin, ycut, ymax: ", ymin, ycut, ymax)
  
  # define regional cuts
  cutA = "(LJmu1_isoID*0.001>="+str(xmin)+"&&LJmu1_isoID*0.001<"+str(xcut)+"&&abs(LJmu1_charge)>="+str(ymin)+"&&abs(LJmu1_charge)<"+str(ycut)+")"
  cutB = "(LJmu1_isoID*0.001>="+str(xmin)+"&&LJmu1_isoID*0.001<"+str(xcut)+"&&abs(LJmu1_charge)>="+str(ycut)+"&&abs(LJmu1_charge)<"+str(ymax)+")"
  cutC = "(LJmu1_isoID*0.001>="+str(xcut)+"&&LJmu1_isoID*0.001<"+str(xmax)+"&&abs(LJmu1_charge)>="+str(ycut)+"&&abs(LJmu1_charge)<"+str(ymax)+")"
  cutD = "(LJmu1_isoID*0.001>="+str(xcut)+"&&LJmu1_isoID*0.001<"+str(xmax)+"&&abs(LJmu1_charge)>="+str(ymin)+"&&abs(LJmu1_charge)<"+str(ycut)+")"
  cutABCD = "(LJmu1_isoID*0.001>="+str(xmin)+"&&LJmu1_isoID*0.001<"+str(xmax)+"&&abs(LJmu1_charge)>="+str(ymin)+"&&abs(LJmu1_charge)<"+str(ymax)+")"

  # errors for TH1 integral
  errA = ctypes.c_double(0)
  errB = ctypes.c_double(0)
  errC = ctypes.c_double(0)
  errD = ctypes.c_double(0)
  errN = ctypes.c_double(0)

  # create histogram and extract nevents for each region ABCD
  canvas = TCanvas("c", "c", 10, 10, 600, 600)
  canvas.SetRightMargin(0.2)
  #canvas.SetLeftMargin(0.15)
  canvas.SetLeftMargin(0.15)
  canvas.SetTopMargin(0.1)
  canvas.cd()
  ttree.Draw("abs(LJmu1_charge):LJmu1_isoID*0.001>>hA", (presel+"*"+cutA+")"), "colz")
  nA = hA.IntegralAndError(0, xbins, 0, ybins, errA)
  
  canvas.Clear()
  canvas.cd()
  ttree.Draw("abs(LJmu1_charge):LJmu1_isoID*0.001>>hB", (presel+"*"+cutB+")"), "colz")
  nB = hB.IntegralAndError(0, xbins, 0, ybins, errB) 
  canvas.Clear()
  
  canvas.cd()
  ttree.Draw("abs(LJmu1_charge):LJmu1_isoID*0.001>>hC", (presel+"*"+cutC+")"), "colz")
  nC = hC.IntegralAndError(0, xbins, 0, ybins, errC)
  canvas.Clear()

  canvas.cd()
  ttree.Draw("abs(LJmu1_charge):LJmu1_isoID*0.001>>hD", (presel+"*"+cutD+")"), "colz")
  nD = hD.IntegralAndError(0, xbins, 0, ybins, errD)
  canvas.Clear()

  canvas.cd()
  # draw histogram of total ABCD plane and save to file
  ttree.Draw("abs(LJmu1_charge):LJmu1_isoID*0.001>>h_"+infile, (presel+"*"+cutABCD+")"), "colz")
  nh = h.IntegralAndError(0, xbins, 0, ybins, errN)
  n = h.IntegralAndError(0, xbins+1, 0, ybins+1, errN)
  gPad.Update()
  h.SetNameTitle(infile, "")
  h.GetXaxis().SetTitle("#muDPJ #Sigma_{#DeltaR=0.5} p_{T} [GeV]") 
  h.SetYTitle("|#muDPJ charge| [e]")
  h.SetZTitle("Events")
  h.GetZaxis().CenterTitle(True)
  h.GetZaxis().SetTitleOffset(1.5)
  h.GetXaxis().SetRange(0, xbins)
  h.GetYaxis().SetRange(0, ybins) 
  if "data" in infile.lower(): h.SetMinimum(1)
  else: h.SetMinimum(0)
  h.GetYaxis().SetRangeUser(ymin, ymax)
  h.GetXaxis().SetRangeUser(xmin, xmax)
  h.GetYaxis().SetNdivisions(5)
  h.GetYaxis().CenterLabels(center=True)
  h.GetXaxis().SetLabelOffset(0.015) 
  #gStyle.SetOptStat(0)
  '''label = TPaveText(9,4.6,20,4.8) # BC region
  label = TPaveText(10,4.6,20,4.75) # DC region
  label = TPaveText(11.5,4.6,20,4.8) # validation region
  label = TPaveText(11.5,4.6,20,4.8) # estimation
  label.SetBorderSize(0)
  label.SetTextSize(0.035)
  label.SetFillStyle(0)
  label.SetTextAlign(10)
  cutStr = TPaveText(2.25,4.38,20,4.53) # VR region
  cutStr = TPaveText(5.25,4.38,20,4.53) # high stat VR region
  cutStr2 = TPaveText(12.75,4.16,20,4.31) # high stat VR region
  cutStr = TPaveText(13.25,4.38,20,4.53) # DC region
  cutStr = TPaveText(12.5,4.4,20,4.6) # BC region
  cutStr.SetBorderSize(0)
  cutStr.SetTextSize(0.035)
  cutStr.SetFillStyle(0)
  cutStr.SetTextAlign(10)
  cutStr2.SetBorderSize(0)
  cutStr2.SetTextSize(0.035)
  cutStr2.SetFillStyle(0)
  cutStr2.SetTextAlign(10)'''
  if mode == "sub-region_validation":
    # BC sub-region validation
    if ymin == 1:
      XC1 = TLatex(0.155,0.29,"BC1")
      XC2 = TLatex(0.155,0.38,"BC2")
      XC3 = TLatex(0.25,0.38,"BC3")
      XC4 = TLatex(0.25,0.29,"BC4")
      #label.AddText("$\\mathrm{BC\\;subplane\\;validation}$");
    # DC sub-region validation
    if xmin == 2:
      XC1 = TLatex(0.155,0.25,"DC1")
      XC2 = TLatex(0.155,0.34,"DC2")
      XC3 = TLatex(0.25,0.34,"DC3")
      XC4 = TLatex(0.25,0.25,"DC4")
      #label.AddText("$\\mathrm{DC\\;subplane\\;validation}$");
    #cutStr.AddText("$\\mathrm{MET>20GeV}$")
    #cutStr.Draw()
    XC1.SetNDC(True)
    XC2.SetNDC(True)
    XC3.SetNDC(True)
    XC4.SetNDC(True)
    XC1.SetTextSize(20)
    XC2.SetTextSize(20)
    XC3.SetTextSize(20)
    XC4.SetTextSize(20)
    XC1.Draw()
    XC2.Draw()
    XC3.Draw()
    XC4.Draw()
  else:
    # ABCD estimation
    if mode == "estimation":
      A = TLatex(0.17,0.22,"A")
      B = TLatex(0.17,0.35,"B")
      C = TLatex(0.25,0.35,"C")
      D = TLatex(0.25,0.22,"D")
    # Control-region validation
    if mode == "control-region_validation":
      A = TLatex(0.17,0.22,"A'")
      B = TLatex(0.17,0.35,"B'")
      C = TLatex(0.25,0.35,"C'")
      D = TLatex(0.25,0.22,"D'")
    A.SetNDC(True)
    B.SetNDC(True)
    C.SetNDC(True)
    D.SetNDC(True)
    A.Draw();
    B.Draw();
    C.Draw();
    D.Draw();
  
  xcut = TLine(xcut, ymin, xcut, ymax)
  xcut.SetLineColor(kRed)
  xcut.SetLineWidth(2)
  ycut = TLine(xmin, ycut, xmax, ycut)
  ycut.SetLineColor(kRed)
  ycut.SetLineWidth(2)
  xcut.Draw("same")
  ycut.Draw("same")
  
  h.SetStats(False)
  if mode == "estimation": 
    if "vbf" in infile.lower(): alabel="Simulation"
    if "data" in infile.lower(): alabel=""
  else: alabel="Work in Progress"
  aplt.atlas_label(x=0.15, y=0.92, text=alabel) 
  lumi = TLatex(0.48,0.83,"#sqrt{s}=13 TeV, 139 fb^{-1}")
  #lumi = TLatex(0.6,0.92,"#sqrt{s}=13 TeV, 139 fb^{-1}")
  lumi.SetTextSize(21)
  lumi.SetNDC(True)
  lumi.Draw()
  print(infile)
  #if "vbf" in infile.lower(): title_txt = "VBF Signal"
  canvas.SetLogz()
  '''if "vbf" in infile.lower(): title_txt = "VBF MC"
  if "data" in infile.lower(): title_txt = "Run-2 data"
  title = TLatex(0.68,0.92,title_txt) # outside plot
  title = TLatex(0.67,0.73,title_txt) # inside plot
  title.SetTextSize(25)
  title.SetNDC(True)
  title.Draw()'''
  if "500758" in infile:
    #sample_txt = TLatex(0.36,0.77,"m_{#gamma_{d}}=0.4 GeV, c#tau_{#gamma_{d}}=50 mm")
    #sample_txt = TLatex(0.46,0.78,"m_{#gamma_{d}}=0.4 GeV, c#tau_{#gamma_{d}}=50 mm")
    #sample_txt = TLatex(0.46,0.83,"m_{#gamma_{d}}=0.4 GeV, c#tau_{#gamma_{d}}=50 mm")
    sample_txt = TLatex(0.37,0.78,"m_{#gamma_{d}}= 0.4 GeV, c#tau_{#gamma_{d}}= 50 mm")
    sample_txt.SetTextSize(21)
    #sample_txt.SetTextSize(25)
    sample_txt.SetNDC(True)
    sample_txt.Draw()
  if "estimation" not in mode and "data" in infile.lower():
    #rho_txt = TLatex(0.65,0.2,"#rho = %.2f" %h.GetCorrelationFactor())
    rho_txt = TLatex(0.6,0.2,"#rho = %.2f" %h.GetCorrelationFactor())
    rho_txt.SetNDC(True)
    rho_txt.Draw()
  #canvas.Print("/Users/s1891044/Documents/Physics/DarkPhotons/vbf-mudpj-abcd-framework/output/charge_isoID/" + mode + "/" + infile + ".png")
  #canvas.Print("/Users/s1891044/Documents/Physics/DarkPhotons/vbf-mudpj-abcd-framework/output/charge_isoID/" + mode + "/" + infile + ".pdf")
  #canvas.Print("/Users/temp/Documents/DarkPhotons/vbf-mudpj-abcd-framework/output/charge_isoID/"+mode+"/"  + infile + ".png")
  canvas.Print("/Users/temp/Documents/DarkPhotons/vbf-mudpj-abcd-framework/output/charge_isoID/"+mode+"/"  + infile + ".pdf")
  print("Plot saved to output/charge_isoID/" + mode + "/" + infile + ".png")
  # return nevents in each region + stat. error
  vals = [nA, nB, nC, nD, n, errA.value, errB.value, errC.value, errD.value, errN.value]
  #print("nA: ", str(round(nA, 1)))
  #print("nB: ", str(round(nB, 1)))
  #print("nC: ", str(round(nC, 1)))
  #print("nD: ", str(round(nD, 1)))
  #if "Data" not in infile: print("n: ", round(n, 1), "±", round(errN.value, 1))
  canvas.Close()
  return vals
