# VBF muDPJ ABCD Framework

ABCD framework to provide a data driven estimate of the number of background events in the VBF muonic DPJ signal region. This is necessary due to the lack of raw MC QCD events generated in the kinematic region of interest for our signal process. No pyenv or specific setup is required.

To run the framework, do $python run.py in the command line. In this file you can specify running mode, and the x and y axis cuts that define the ABCD plane. 
There are four modes built in:
\1. Estimation: To perform the ABCD estimation in the signal region (mode=='estimation'), which will run over signal + data samples
\2. Correlation: To extract a correlation from either QCD or data OUTSIDE the signal region
\3. Sub-region validation: Validate the method in a sub-region of the ABCD plane by combining two background enriched regions. This will run over the data sample only
\4. Control-region validation: Validation the method in a region orthogonal to the signal region. Typically this is achieved by inverting one SR cut, and will be run over signal as well as data, to check that the signal contribution in this control region is negligible.

The running script run.py will call the program runABCD.py which takes four arguments:
- inFile: the path to the ROOT file to run over
- sample: 'VBF 5007XX', 'Data' or 'QCD' (XX to be replaced with VBF sample in question)
- mode (as defined above)
- cuts (as defined above)

The program runABCD.py calls charge\_isoID.py to create a histogram in each ABCD sub-region and returns the integral. A histogram of the entire ABCD plane with a red line denoting the value of the cuts in x and y is saved to output/plots. Depending on the sample type, runABCD.py will either calculate signal leakage into the control regions (sample=='VBF 5007XX'), or calculate the estimate for number of background events in the signal region (sample=='Data') or extract a correlation (sample=='QCD') The yields are returned to the main run.py and given as argument to the program writeCSV.py which creates a csv with the extracted yields in each region for each cut.

The output table is written to output/tables which you will have to create yourself along with output/plots as all my outputs are in my .gitignore. The current setup is for the LJmu1 |charge| vs. LJmu1 isoID ABCD plane however other scripts defining ABCD planes that have not worked exist inside the .gitignore. These are LJmu1 isoID vs.:
- |dphijj|
- |detajj|
- LJmu1 DNNscore
- LJmu1 centrality

Other helper files are:
 
addBranch.py: This script creates a copy of existing ntuples, adding other necessary branches, in this case to define new control regions. For example, the branches LJmu1\_centrality and LJmu1\_charge are defined in here and added the ntuple in this way. 

plot\_expVsObs.py: This script runs the ABCD framework for each value in a list of x/y cuts in one of the two validation modes (this should be specified in the function baring the name of the variable sliding the cut over) and creates a plot of two lines: the expected vs. observed number of events in data in the signal region as a function for each value of the variable, with an error bar showing the statistical error on the expected value. For the ABCD plane to close, we want all observed to fall within the error band of the expected.
 
