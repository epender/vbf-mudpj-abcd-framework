# Program runs ABCD framework
# Write yields and estimate to csv file

import csv
import numpy as np
import runABCD
import getRow 
from csv import reader, writer

def main(mode, cuts):

  #wdir = "/Volumes/Extreme SSD/v02-00/miniT/is_jvtLoose/"
  wdir = "/Volumes/Extreme SSD/v02-00/miniT/"
  #wdir = "/Users/s1891044/Documents/Physics/DarkPhotons/dljntuplereader/output/vbfdresser/"
  outdir = "/Users/s1891044/Documents/Physics/DarkPhotons/vbf-mudpj-abcd-framework/output/charge_isoID/"+mode+"/"
  
  files = ["frvz_vbf_500758.root", "data_run2.root"]
  #files = ["frvz_vbf_521257.root", "frvz_vbf_521258.root", "frvz_vbf_521259.root", "frvz_vbf_521260.root", "frvz_vbf_521261.root", "frvz_vbf_521262.root", "frvz_vbf_521263.root", "frvz_vbf_500757.root", "frvz_vbf_500758.root", "frvz_vbf_500761.root", "frvz_vbf_500762.root", "data_run2.root"]
  #files = ["frvz_vbf_521257.root", "frvz_vbf_521258.root", "frvz_vbf_500757.root", "frvz_vbf_500758.root", "frvz_vbf_521259.root", "frvz_vbf_521260.root"]
  #files = ["frvz_vbf_521261.root", "frvz_vbf_500761.root", "frvz_vbf_500762.root", "frvz_vbf_521262.root", "frvz_vbf_521263.root", "data_run2.root"]
  #files = ["data_run2.root"]
  #yields = []
  with open(outdir+"output.csv", "w") as outfile:#, open(outdir+"signal_fraction.csv", "w") as outfile2:
    writer = csv.writer(outfile)
    writer.writerow([mode, "nA", "nB", "nC", "nD", "nA estimate"])
    #writer2 = csv.writer(outfile2)
    #writer2.writerow(["Signal Fraction", "A", "B", "C", "D"])
    for idx in range(len(files)):
      infile = wdir + files[idx]
      sample = files[idx].replace(".root", "").replace("frvz_", "").replace("_", " ")
      if "data" in sample: sample = "Run 2 Data"
      if "qcd" in sample: sample = "QCD"
      yields = runABCD.main(infile, mode, sample, cuts)
      '''if "vbf" in files[idx]: 
        # calculate signal fraction in each region
        n = (yields[0] + yields[1] + yields[2] + yields[3])
        sfA = str(round(yields[0]/n, 2))
        sfB = str(round(yields[1]/n, 2))
        sfC = str(round(yields[2]/n, 2))
        sfD = str(round(yields[3]/n, 2))
        row2 = [sample, sfA, sfB, sfC, sfD]
        writer2.writerow(row2)'''
      row = getRow.main(yields, mode, sample)
      writer.writerow(row)
  # tranpose table to add to cutflow
  with open(outdir+'output.csv', "r") as f, open(outdir+'output_T.csv', 'w', newline='') as fT:
    writer = csv.writer(fT)
    writer.writerows(zip(*reader(f)))
  '''with open(outdir+'signal_fraction.csv', "r") as f2, open(outdir+'signal_fraction_T.csv', 'w', newline='') as fT2:
    writer2 = csv.writer(fT2)
    writer2.writerows(zip(*reader(f2)))'''

  print("\n")
  print("Table with ABCD yields written to output/charge_isoID/" + mode + "/output.csv") 
  print("\n")
  print("Program exiting...")
  print("------------------")
  print("\n")

main('estimation', cuts=[2, 1])
#main('sub-region_validation', cuts=[2, 2]) # BC sub-region
#main('sub-region_validation', cuts=[4, 1]) # DC sub-region
#main('control-region_validation', cuts=[2, 1])
